﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using whatishappening.Models;
using System.Collections.ObjectModel;

namespace whatishappening
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            PopulateColorList();
        }

        ObservableCollection<ImageCellItems> collection = new ObservableCollection<ImageCellItems>();


        public void PopulateColorList()
        {


             var item1 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("ie.jpg"),
                ImageText = "IFINITY EDGE",
                moreInfo = "Attack Damage:80\n\n" +
                   "CRITICAL STRIKE: 25%\n\n" +
                   "ATTACK SPEED: 0%\n\n" +
                   "COST:3400g \n\n" +
                   "PASSIVE: YOUR CRITICAL STRIKES NOW DEAL 250% ",
                Details = "gives increased critical hit damage.",
                wiki = "Learn more!",
                url = "http://leagueoflegends.wikia.com/wiki/Infinity_Edge"
            };

            var item2 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("pd.png"),
                ImageText = "PHANTOM DANCER",
                moreInfo = "Attack Damage:0\n\n" +
                "CRITICAL STRIKE: 30%\n\n" +
                "ATTACK SPEED: 45%\n\n" +
                "COST:2600g \n\n" +
                "PASSIVE: 5% movement speed\n " +
                "+7% if 550 units near a visible enemy champion\n" +
                "12% dmg reduction against the last enemy champion hit",
                Details = "gives attack speed and dueling power.",
                wiki = "Learn more!!",
                url = "http://leagueoflegends.wikia.com/wiki/Phantom_Dancer"


            };

            var item3 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("bt.png"),
                ImageText = "BLOOD THIRSTER",
                moreInfo = "Attack Damage:80\n\n" +
                "CRITICAL STRIKE: 0%\n\n" +
                "ATTACK SPEED: 0%\n\n" +
                "COST:3700g \n\n" +
                "PASSIVE: 20% life steal \n " +
                "Your life steal overheals you, converting the excess healing into a sheild that absorts up to 50-350(based on level) damage and decays while out of combat for 25 seconds",
                Details = "gives lifesteal on attacks.",
                wiki = "Learn more!",
                url = "http://leagueoflegends.wikia.com/wiki/The_Bloodthirster"

            };

            var item4 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("trinity.jpg"),
                ImageText = "TRINITY FORCE",
                moreInfo = "Attack Damage:25\n\n" +
                "CRITICAL STRIKE: 0%\n\n" +
                "ATTACK SPEED: 40%\n\n" +
                "CDR: 20%\n\n" +
                "HEALTH: 250\n\n" +
                "MANA: 250\n\n" +
                "MOVESPEED: 5%\n\n" +
                "COST:3733g \n\n" +
                "PASSIVE: After using an ability, your next basic attack within 10 seconds deals (200% base AD) on-hit bonus damage (1.5 second cooldown)\n " +
                "Basic Attacks grant 20 movespeed for 2 seconds. Killing a unit grants 60 instead. Bonus movment is halved for ranged champions",
                Details = "gives a myriad of stats.",
                wiki = "Learn more!",
                url = "http://leagueoflegends.wikia.com/wiki/Trinity_Force"
            };

            var item5 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("shiv.jpg"),
                ImageText = "STATIC SHIV",
                moreInfo = "Attack Damage:0\n\n" +
                "CRITICAL STRIKE: 30%\n\n" +
                "ATTACK SPEED: 35%\n\n" +
                "MOVESPEED: 5%\n\n " +
                "COST:2600g \n\n" +
                "PASSIVE: Moving and attacking generates Energize stacks, up to 100. When fully charged yor next basic attack grants SHIV LIGHTNING \n\n " +
                "SHIV LITNING: Your basic attack sparks litning up to 5 nearby units dealing 60-160 (based on level) increased to 99 -264 (based on level) against minions. \n" +
                "SHIVLIGHTNING can critically strikes dealing 100% (150% if i.e is equiped) against minions",
                Details = "gives an aoe on attacks.",
                wiki = "Learn more!",
                url = "http://leagueoflegends.wikia.com/wiki/Statikk_Shiv"
            };

            var item6 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("er.jpg"),
                ImageText = "ESSENCE REAVER",
                moreInfo = "Attack Damage:70\n\n" +
                "CRITICAL STRIKE: 20%\n\n" +
                "ATTACK SPEED: 0%\n\n" +
                "CDR: 10% \n\n" +
                "COST:3400g \n\n" +
                "PASSIVE: YOUR CRITICAL STRIKES restore 3% of your maximum mana \n " +
                "gain 0 -20% CDR based on additional critical strike chance (critical strike chance from this item is not included)",
                Details = " gives critical strike chance and CDR.",
                wiki = "Learn more!",
                url = "http://leagueoflegends.wikia.com/wiki/Essence_Reaver"
            };

            var item7 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("mr.png"),
                ImageText = "MORTAL REMINDER",
                moreInfo = "Attack Damage:50\n\n" +
                "CRITICAL STRIKE: 0%\n\n" +
                "ATTACK SPEED: 0%\n\n" +
                "COST:2600g \n\n" +
                "PASSIVE: 35% bonus armor penetrations \n " +
                "EXECUTIONER: physical damage inflicts Grevious Wounds on enemy champions for 5 seconds (reduces healing by 40%)",
                Details = "reduces healing on targets and penetrates armor.",
                wiki = "Learn more!",
                url = "http://leagueoflegends.wikia.com/wiki/Mortal_Reminder"
            };

            var item8 = new ImageCellItems()
            {
                IconSource = ImageSource.FromFile("bork.jpg"),
                ImageText = "BLADE OF THE \nRUINED KING",
                moreInfo = "Attack Damage:40\n\n" +
                "CRITICAL STRIKE: 0%\n\n" +
                "ATTACK SPEED: 25%\n\n" +
                "LIFESTEAL: 12% \n\n" +
                "COST:3400g \n\n" +
                "PASSIVE: Basic attackds deal 8% of the targets current health(min 15) bonus on hit physical damage (max 60 against minions and monsters)\n " +
                "ACTIVE: Deal 100 magic damage and steal 25% of the targets movement speed for 3 seoconds (550 range) (90 seconds cooldown)",
                Details = "gives % health damage.",
                wiki = "Learn more!",
                url = "http://leagueoflegends.wikia.com/wiki/Blade_of_the_Ruined_King"
            };

            collection.Add(item1);
            collection.Add(item2);
            collection.Add(item3);
            collection.Add(item4);
            collection.Add(item5);
            collection.Add(item6);
            collection.Add(item7);
            collection.Add(item8);

            ImageCellsListView.ItemsSource = collection;

        }


    
    void Handle_More(object sender, System.EventArgs e)
    {
        var menuItem = (MenuItem)sender;

        var Item = (ImageCellItems)menuItem.CommandParameter;

        Navigation.PushAsync(new moreInfoPage(Item));
    }


     void Handle_Delete(object sender, System.EventArgs e)
     {
            var item = ((MenuItem)sender);

            var model = (ImageCellItems)item.CommandParameter;

            

            collection.Remove(model);


     }

     void Handle_Refreshing(object sender, System.EventArgs e)
     {
            collection.Clear();
            PopulateColorList();
            ImageCellsListView.IsRefreshing = false;
         
     }





    }
}










