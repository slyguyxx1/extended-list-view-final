﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace whatishappening.Models
{
    public class ImageCellItems
    {
        public string ImageText
        {
            get;
            set;
        }

        public ImageSource IconSource
        {
            get;
            set;
        }

        public string Details
        {
            get;
            set;
        }

        public string websiteName
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }

        public string moreInfo
        {
            get;
            set;
        }

        public string wiki
        {
            get;
            set;
        }




    }
}

