﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using whatishappening.Models;

namespace whatishappening
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class moreInfoPage : ContentPage
	{
		public moreInfoPage ()
		{
			InitializeComponent ();
		}

        public moreInfoPage(ImageCellItems item)
        {
            InitializeComponent();

            BindingContext = item;
        }

        void Handle_url(object sender, EventArgs e)
        {

            var view = (Button)sender;

            ImageCellItems itemSelect = (ImageCellItems)view.CommandParameter;

            var uri = new Uri(itemSelect.url);

            Device.OpenUri(uri);

        }

    }
}